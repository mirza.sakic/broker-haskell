module Main where

import Control.Monad.State 
import Text.Megaparsec (runParser, errorBundlePretty)
import Text.Read (readMaybe)
import System.IO (isEOF, stderr, hPutStr)
import System.Environment

import Parse (parseLine)
import DataModel
import OrderBook (OrderBook, constructEmptyOrderBook)
import BrokerLogic (calculateAndPrint, BrokerOutputState, constructEmptyBrokerState)

import Data.Text as T

readMessage :: IO (Maybe ParsedMessage)
readMessage = do
  line <- Prelude.getLine
  let result = runParser parseLine "" (T.pack line)
   in 
    case result of
    Left a -> do
      let errorMessage = "Error during parsing of input: \n " ++ (errorBundlePretty a) ++ "\n"
      _ <- hPutStr stderr errorMessage
      return Nothing
    Right a -> return (Just a)

brokerProgram :: Integer -> StateT (OrderBook, BrokerOutputState) IO ()
brokerProgram qq = do
  done <- liftIO isEOF
  if done
     then pure ()
     else do
          maybeParsedMessage <- liftIO readMessage
          (orderBook, output) <- get
          case maybeParsedMessage of
            Nothing -> brokerProgram qq
            Just parsedMessage -> do
              newState <- liftIO $ calculateAndPrint qq (orderBook, output) parsedMessage
              _ <- put newState
              brokerProgram qq

validateArg :: [String] -> Integer
validateArg [] = error "Please provide input argument"
validateArg (x:xs) = let maybeQQ = readMaybe x 
                      in case maybeQQ of
                           Nothing -> error "Input argument must be an Integer!"
                           Just qq -> qq

main :: IO ()
main = do 
  args <- getArgs
  let qq = validateArg args
      programState = brokerProgram qq
  _ <- runStateT programState (constructEmptyOrderBook, constructEmptyBrokerState)
  pure ()

