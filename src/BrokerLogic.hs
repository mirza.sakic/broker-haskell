module BrokerLogic where

import DataModel
import OrderBook
import Data.Text (Text)
import Data.Text as T
import Data.Text.IO as TIO
import Data.Double.Conversion.Text
import Data.Fixed
import  Control.Monad.State (runState)

data OutputMessage = OutputMessage {
                                     oTime :: Integer
                                   , oSide :: Side
                                   , oAmount :: Maybe Text
                                   }

instance Eq OutputMessage where
  x == y = oAmount x == oAmount y

outputMessageToText :: OutputMessage -> Text
outputMessageToText msg = T.pack(show (oTime msg) ++ " " ++ outputSide ++ " ") <> outputAmount
  where outputSide = case oSide msg of
                       Buy -> "B"
                       Sell -> "S"
        outputAmount = case oAmount msg of
                         Nothing -> T.pack "NA"
                         Just amt -> amt

type BrokerOutput = Maybe OutputMessage
type BrokerOutputState = (BrokerOutput, BrokerOutput)

setStateIfDiff :: Maybe OutputMessage -> OutputMessage -> (Bool, BrokerOutput)
setStateIfDiff outputState newOutput =
  case outputState of
    Nothing -> (True, Just newOutput)
    Just oldOutput -> case (oldOutput == newOutput) of
                        True -> (False, Just oldOutput)
                        False -> (True, Just newOutput)

printStateIfDiff :: Maybe OutputMessage -> OutputMessage -> IO (Bool, BrokerOutput)
printStateIfDiff outputState newOutput = 
  let (isNewOutputDiff, newState) = setStateIfDiff outputState newOutput
   in if isNewOutputDiff
         then case newState of
                Nothing -> error "Will never happen"
                Just newOutput -> do
                  _ <- TIO.putStrLn $ outputMessageToText newOutput
                  return (isNewOutputDiff, newState)
         else return (isNewOutputDiff, newState)

calculateOutput :: (Order a) => Integer -> Integer -> Side -> OrderBookSide a -> BrokerOutput -> IO (Bool, BrokerOutput)
calculateOutput minQuantity time side bookSide sideOutput =
  let stateFunction = calculateAmount minQuantity bookSide
      (isAmountCalculated, amount) = runState (stateFunction) 0
   in if isAmountCalculated
         then printStateIfDiff sideOutput (OutputMessage time side (toFixed 2 <$> Just amount))   
         else case sideOutput of
                Nothing -> return (False, Nothing)
                Just output -> printStateIfDiff sideOutput (OutputMessage time side Nothing)

calculateBasedOnSide :: Integer -> Integer -> (OrderBook, BrokerOutputState) -> Side -> IO (Bool, BrokerOutputState)
calculateBasedOnSide minQuantity time ((buySide, sellSide), (buyOutput, sellOutput)) lastSide = do
        case lastSide of
          Buy -> do 
            (isCalculated, newBuyOutput) <- calculateOutput minQuantity time Sell buySide buyOutput
            return (isCalculated, (newBuyOutput, sellOutput))
          Sell -> do 
            (isCalculated, newSellOutput) <- calculateOutput minQuantity time Buy sellSide sellOutput
            return (isCalculated, (buyOutput, newSellOutput))

calculateAndPrint :: Integer -> (OrderBook, BrokerOutputState) -> ParsedMessage -> IO (OrderBook, BrokerOutputState)
calculateAndPrint qq (orderBook, output) parsedMessage = do
  let typeOfMsg = msgType parsedMessage
   in case typeOfMsg of 
                Reduce -> do
                  let (orderSide, newOrderBook) = reduceOrderBook parsedMessage orderBook
                  (_, newOutput) <- calculateBasedOnSide qq (msgTime parsedMessage) (newOrderBook, output) orderSide
                  return (newOrderBook, newOutput)
                Add -> do
                  let newOrderBook = insertInOrderBook parsedMessage orderBook
                      parsedMessageSide = case maybeSide parsedMessage of
                                                Nothing -> error "Will never happen"
                                                Just ff -> ff
                  (b, newOutput) <- calculateBasedOnSide qq (msgTime parsedMessage) (newOrderBook, output) parsedMessageSide
                  return (newOrderBook, newOutput)

constructEmptyBrokerState :: BrokerOutputState
constructEmptyBrokerState = (Nothing, Nothing)
