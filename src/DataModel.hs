module DataModel where

import Data.Text (Text)

data Side = Buy | Sell deriving (Show, Eq)

data MsgType = Add | Reduce deriving (Show, Eq)

data ParsedMessage = ParsedMessage {
                  msgTime :: Integer
                , msgType :: MsgType
                , orderId :: Text
                , maybeSide :: Maybe Side
                , maybePx :: Maybe Double
                , maybeQty :: Maybe Integer
                , maybeDelta :: Maybe Integer
                } deriving Show

class (Eq a, Ord a) => Order a where
  ordTime :: a -> Integer
  ordID :: a -> Text
  ordPx :: a -> Double
  ordQty :: a -> Integer
  ordSide :: a -> Side
  changeQty :: Integer -> a -> a
  convertParsedMessage :: ParsedMessage -> a
  getOrderFromId :: Text -> a

data BuyOrder = BuyOrder {
                     bTime :: Integer
                   , bID :: Text
                   , bPx :: Double
                   , bQty :: Integer
                   } deriving (Show)

instance Order BuyOrder where
  ordTime = bTime
  ordID = bID
  ordPx = bPx
  ordQty = bQty
  ordSide _ = Buy
  changeQty newQty ord =
    BuyOrder (ordTime ord) (ordID ord) (ordPx ord) newQty
  convertParsedMessage pMsg = BuyOrder (msgTime pMsg) (orderId pMsg) msgPx msgQty
                                where msgPx = case maybePx pMsg of
                                        Nothing -> error "Will never happen"
                                        Just a -> a
                                      msgQty = case maybeQty pMsg of
                                        Nothing -> error "Will never happen"
                                        Just a -> a
  getOrderFromId id = BuyOrder 0 id 0 0



instance Eq BuyOrder where
  x == y = bID x == bID y

instance Ord BuyOrder where
  x <= y = bPx x >= bPx y

data SellOrder = SellOrder {
                     sTime :: Integer
                   , sID :: Text
                   , sPx :: Double
                   , sQty :: Integer
                   } deriving (Show)

instance Eq SellOrder where
  x == y = sID x == sID y

instance Ord SellOrder where
  x <= y = sPx x <= sPx y

instance Order SellOrder where
  ordTime = sTime
  ordID = sID
  ordPx = sPx
  ordQty = sQty
  ordSide _ = Sell
  changeQty newQty ord =
    SellOrder (ordTime ord) (ordID ord) (ordPx ord) newQty 
  convertParsedMessage pMsg = SellOrder (msgTime pMsg) (orderId pMsg) msgPx msgQty
                                 where msgPx = case maybePx pMsg of
                                         Nothing -> error "Will never happen"
                                         Just a -> a
                                       msgQty = case maybeQty pMsg of
                                        Nothing -> error "Will never happen"
                                        Just a -> a
  getOrderFromId id = SellOrder 0 id 0 0

data ModifyDown = ModifyDown {
                               mTime :: Integer
                             , mID :: Text
                             , mDelta :: Integer
                             } deriving (Show)

convertParsedMessageToModifyDown :: ParsedMessage -> ModifyDown
convertParsedMessageToModifyDown msg = ModifyDown (msgTime msg) (orderId msg) delta 
                              where delta = case maybeDelta msg of
                                              Nothing -> error "Will never happen"
                                              Just a -> a



