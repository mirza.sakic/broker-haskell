module OrderBook where

import Data.SortedList as SL
import Data.Text (Text)
import DataModel
import Control.Monad.State

type OrderBookSide a = SortedList a

type OrderBook = (OrderBookSide BuyOrder, OrderBookSide SellOrder)
  
insertInOrderBook :: ParsedMessage -> OrderBook -> OrderBook 
insertInOrderBook parsedMessage (entryBuy, entrySell) = 
  case maybeSide parsedMessage of
    Nothing -> error "No side!"
    Just side -> 
      if side == Buy
         then let returnBuy = insert (convertParsedMessage parsedMessage) entryBuy
               in (returnBuy, entrySell)
         else let returnSell = insert (convertParsedMessage parsedMessage) entrySell
               in (entryBuy, returnSell)

reduceOrder :: (Order a) => Integer -> a -> a
reduceOrder delta order = changeQty (ordQty order - delta) order

deleteOrReduceOrderBookSide :: (Order a) => OrderBookSide a -> ModifyDown -> OrderBookSide a
deleteOrReduceOrderBookSide bookSide modifyDown = 
 let filteredSide = SL.filter (\x -> ordID x == mID modifyDown) bookSide
  in case uncons filteredSide of
      Nothing -> bookSide
      Just (x, xs) -> 
        if ordQty x <= mDelta modifyDown
          then let newBookSide = delete (getOrderFromId (mID modifyDown)) bookSide
                in newBookSide
          else let newBookSide = SL.map (\x -> 
                                        if ordID x == mID modifyDown 
                                           then let deltaQty = mDelta modifyDown
                                                 in reduceOrder deltaQty x
                                           else x) bookSide
             in newBookSide

reduceSell :: Integer -> SellOrder -> SellOrder
reduceSell delta order = SellOrder (sTime order) (sID order) (sPx order) ((sQty order) - delta)

reduceBuy :: Integer -> BuyOrder -> BuyOrder
reduceBuy delta order = BuyOrder (bTime order) (bID order) (bPx order) ((bQty order) - delta)

findOrderInBookSide :: (Order a) => Text -> OrderBookSide a -> OrderBookSide a
findOrderInBookSide id = SL.filter (\x -> ordID x == id)

isOrderBookSideEmpty :: (Order a) => OrderBookSide a -> Bool
isOrderBookSideEmpty bookSide = bookSide == toSortedList []

reduceOrderBook :: ParsedMessage -> OrderBook -> (Side, OrderBook)
reduceOrderBook message (buySide, sellSide) =
  let modifyDown = convertParsedMessageToModifyDown message
      filteredBuy = findOrderInBookSide (mID modifyDown) buySide
   in if isOrderBookSideEmpty filteredBuy
         then let filteredSell = findOrderInBookSide (mID modifyDown) sellSide
                  reducedSell = deleteOrReduceOrderBookSide sellSide modifyDown
               in (Sell, (buySide, reducedSell))
         else let reducedBuy = deleteOrReduceOrderBookSide buySide modifyDown
               in (Buy, (reducedBuy, sellSide))

type Amount = Double

calculateAmount :: (Order a) => Integer -> OrderBookSide a -> State Amount Bool
calculateAmount 0 _ = return True
calculateAmount wantedQty bookSide =
  case uncons bookSide of
    Nothing -> return False
    Just (x, xs) -> do
      currentAmount <- get
      let remQty = wantedQty - ordQty x
       in case remQty >= 0 of
            True -> do
              _ <- put $ fromIntegral (ordQty x) * (ordPx x) + currentAmount
              calculateAmount remQty xs
            False -> do
              _ <- put $ fromIntegral (wantedQty) * (ordPx x) + currentAmount
              return True

constructEmptyOrderBook :: OrderBook
constructEmptyOrderBook = (toSortedList [], toSortedList [])

