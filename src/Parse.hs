{-# LANGUAGE OverloadedStrings #-}
module Parse where

import Text.Megaparsec hiding (State)
import Text.Megaparsec.Char
import Data.Text (Text)
import Data.Void
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Data.Text as T
import DataModel


type Parser = Parsec Void Text

justSpace :: Parser ()
justSpace = space1

lexeme :: Parser a -> Parser a
lexeme = L.lexeme space

integer :: Parser Integer
integer = lexeme L.decimal

parseTime :: Parser Integer
parseTime = lexeme L.decimal

parseMsgType :: Parser MsgType
parseMsgType = choice
      [ Add <$ char 'A'
      , Reduce <$ char 'R' ]

parseOrdId :: Parser Text
parseOrdId = T.pack <$> some alphaNumChar

parseSide :: Parser Side
parseSide = choice 
      [ Buy <$ char 'B'
      , Sell <$ char 'S' ]

parsePrice :: Parser Double
parsePrice = L.float

parseQuantity :: Parser Integer
parseQuantity = L.decimal

parseDelta :: Parser Integer
parseDelta = L.signed justSpace integer

parseAddMessage :: Integer -> Parser ParsedMessage
parseAddMessage time = do
  orderID <- parseOrdId
  _ <- justSpace
  side <- parseSide
  _ <- justSpace
  px <- parsePrice
  _ <- justSpace
  qty <- parseQuantity
  return (ParsedMessage time Add orderID (Just side) (Just px) (Just qty) Nothing)

parseModifyDown :: Integer -> Parser ParsedMessage
parseModifyDown time = do
  orderID <- parseOrdId
  _ <- justSpace
  delta <- parseDelta
  return (ParsedMessage time Reduce orderID Nothing Nothing Nothing (Just delta))


parseLine :: Parser ParsedMessage 
parseLine = do
  time <- parseTime
  msgType <- parseMsgType
  _ <- justSpace
  let result | msgType == Add = parseAddMessage time
             | msgType == Reduce = parseModifyDown time
  result


